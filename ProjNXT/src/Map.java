import java.util.ArrayList;

public class Map {
	private ArrayList<MapPoint> points;
	
	public void update(MapPoint newPoint) {
		for (MapPoint p : points) {
			if (p.positionX == newPoint.positionX && p.positionY == newPoint.positionX) {
				p.isLeftBlocked = newPoint.isLeftBlocked;
				p.isTopBlocked = newPoint.isTopBlocked;
				p.isRightBlocked = newPoint.isRightBlocked;
				p.isBottomBlocked = newPoint.isBottomBlocked;
				
				return;
			}
		}
		
		points.add(newPoint);
	}
}
