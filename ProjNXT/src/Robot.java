import lejos.nxt.SensorPort;
import lejos.nxt.UltrasonicSensor;


public class Robot {
	private int orientation;
	private int positionX;
	private int positionY;
	
	private UltrasonicSensor usSensor;
	
	public Robot() {
		usSensor = new UltrasonicSensor(SensorPort.S1);
	}
	
	public void moveForward() {
		if (orientation == 0) {
			positionY += 1;
		} else if (orientation == 1) {
			positionX += 1;
		} else if (orientation == 2) {
			positionY -= 1;
		} else if (orientation == 3) {
			positionX -= 1;
		}
	}
	
	public void turnLeft() {
		orientation--;
		
		if (orientation < 0) orientation += 4;
	}
	
	public void turnRight() {
		orientation++;
		
		orientation = orientation % 4;
	}
	
	public MapPoint scanPosition() {
		MapPoint p = new MapPoint();
		p.positionX = positionX;
		p.positionY = positionY;
		
		for (int i = 0; i < 3; i++) {
			int dist = usSensor.getDistance();
			updateBlockedStatus(p, i, (dist < 30));
		}
		
		return p;
	}
	
	private void updateBlockedStatus(MapPoint p, int direction, boolean isBlocked) {
		int realDirection = (orientation + direction)%4;
		
		switch (realDirection) {
			case 0:
				p.isLeftBlocked = true;
				break;
			case 1:
				p.isTopBlocked = true;
				break;
			case 2:
				p.isRightBlocked = true;
				break;
			case 3:
				p.isBottomBlocked = true;
				break;
		}
	}
}
