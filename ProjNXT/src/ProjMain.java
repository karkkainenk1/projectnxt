import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.comm.BTConnection;
import lejos.nxt.comm.Bluetooth;


public class ProjMain {
	public static void main(String[] args) throws Exception {
		ProjMain projMain = new ProjMain();
		projMain.start();
	}
	
	
	private Robot robot;
	private Map map;
	private BTConnection bluetooth;
	private DataOutputStream outputStream;
	
	public ProjMain() {
		robot = new Robot();
		map = new Map();

        bluetooth = Bluetooth.waitForConnection();
		OutputStream os = bluetooth.openOutputStream();
		outputStream = new DataOutputStream( os );
	}
	
	public void start() throws Exception {
		mapArea();
	}
	
	private MapPoint getPositionData() throws Exception {
		MapPoint p = robot.scanPosition();
		
		outputStream.writeInt( p.positionX );
		outputStream.flush();
		
		outputStream.writeInt( p.positionY );
		outputStream.flush();
		
		outputStream.writeBoolean( p.isLeftBlocked );
		outputStream.flush();
		
		outputStream.writeBoolean( p.isTopBlocked );
		outputStream.flush();
		
		outputStream.writeBoolean( p.isRightBlocked );
		outputStream.flush();
		
		outputStream.writeBoolean( p.isBottomBlocked );
		outputStream.flush();
		
		return p;
	}
	
	private void mapArea() throws Exception {
		for (int i = 0; i < 2; i++) {
			robot.moveForward();
			MapPoint p = getPositionData();
			map.update(p);
		}
		
		robot.turnRight();
		
		for (int i = 0; i < 2; i++) {
			robot.moveForward();
			MapPoint p = getPositionData();
			map.update(p);
		}
	}
}
